package client

import (
	"gitee.com/yangzx6606/core/pattern"
	"net"
)

type ClientImplOnRTP struct {
	pattern.AbstractSubject[[]byte]
	id      string
	address string
	conn    net.PacketConn
}

func NewRtpClient(id string, address string) *ClientImplOnRTP {
	return &ClientImplOnRTP{id: id, address: address}
}

func (c *ClientImplOnRTP) Open() error {

	return nil
}

func (c *ClientImplOnRTP) Close() {

}

func (c *ClientImplOnRTP) Write(data []byte) error {
	return nil
}
