package pb

import (
	"encoding/hex"
	"google.golang.org/protobuf/types/known/anypb"

	"testing"
)

func TestMarshalMessagePacket(t *testing.T) {
	pkt := Packet{Type: PacketType_PacketType_BinaryFrame}
	content := BinaryFrame{Content: []byte{0x01, 0x02, 0x03}}
	var err error
	pkt.Data, err = anypb.New(&content)
	if err != nil {
		t.Error(err)
	}
	frame, err := MarshalMessagePacket(&pkt)
	t.Log("frame=", hex.EncodeToString(frame))
}

func TestMessagePacketParser_PushData(t *testing.T) {
	//data, err := hex.DecodeString("ffffffff00000037080212330a26747970652e676f6f676c65617069732e636f6d2f436f6d6d616e6450756c6c5265717565737412090a072f73747265616d")
	//if err != nil {
	//	t.Fatal("解析16进制失败")
	//}
	//data = append(data, data...)
	//parser := &MessagePacketParser{}

}
