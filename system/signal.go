package system

import (
	"os"
	"os/signal"
	"strings"
)

var exitSignals = [...]string{
	2:  "interrupt",
	3:  "quit",
	4:  "illegal instruction",
	6:  "aborted",
	8:  "floating point exception",
	9:  "killed",
	15: "terminated",
}

func SignalWait() {
	sign := make(chan os.Signal)
	signal.Notify(sign)
	for {
		isExit := false
		select {
		case sign := <-sign:
			for _, v := range exitSignals {
				if strings.Compare(v, sign.String()) == 0 {
					isExit = true
					break
				}
			}
			break
		}
		if isExit {
			break
		}
	}
}
