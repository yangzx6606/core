package pattern

// 观察者模式

// Subject 抽象主题
type Subject[T any] interface {
	AttachObserver(ob Observer[T], data any)
	DetachObserver(ob Observer[T])
	Notify(data T)
}

// Observer 观察者
type Observer[T any] interface {
	OnData(data T, args any)
}

// AbstractSubject 实现了观察者主题功能，使用时继承使用
type AbstractSubject[T any] struct {
	observers map[Observer[T]]any
}

func (s *AbstractSubject[T]) AttachObserver(ob Observer[T], data any) {
	if s.observers == nil {
		s.observers = make(map[Observer[T]]any)
	}
	s.observers[ob] = data
}

func (s *AbstractSubject[T]) DetachObserver(ob Observer[T]) {
	delete(s.observers, ob)
}

func (s *AbstractSubject[T]) Notify(data T) {
	for k, v := range s.observers {
		k.OnData(data, v)
	}
}
