package media

import (
	"gitee.com/yangzx6606/core/pattern"
	"gitee.com/yangzx6606/core/pb"
)

type VideoPacketType int

const (
	VideoPacketTypeConnected   VideoPacketType = 0 // 连接成功通知
	VideoPacketTypeConnectLost VideoPacketType = 1 // 连接断开通知

	VideoPacketTypeKeyFrame VideoPacketType = 2 // 关键帧通知
	VideoPacketTypeFrame    VideoPacketType = 3 // 普通帧通知
	VideoPacketTypeCodec    VideoPacketType = 4 // PPS SPS通知
)

type VideoPacket struct {
	Type   VideoPacketType // 包类型
	Packet *pb.Packet      // 包内容
}

type Puller interface {
	Open() error
	Close()
	pattern.Subject[*VideoPacket]
}
